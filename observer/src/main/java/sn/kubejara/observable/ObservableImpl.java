package sn.kubejara.observable;

import sn.kubejara.observer.Observer;

import java.util.ArrayList;
import java.util.List;

public class ObservableImpl implements Observable{

    private int state =10;
    private List<Observer> observers = new ArrayList<>();
    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void deleteObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        this.observers.forEach(observer -> observer.update(this));
    }

    public void setState(int state) {
        this.state = state;
        this.notifyObservers();
    }

    public int getState() {
        return state;
    }
}
