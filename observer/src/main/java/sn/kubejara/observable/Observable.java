package sn.kubejara.observable;

import sn.kubejara.observer.Observer;

/**
 *  Cette classe est aussi appelée "SUJET"
 */
public interface Observable {
    void addObserver(Observer observer); // subscribe(Observer observer)
    void deleteObserver(Observer observer); // unsubscribe(Observer observer)
    void notifyObservers();

}
