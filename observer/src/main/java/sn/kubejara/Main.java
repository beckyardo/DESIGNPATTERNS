package sn.kubejara;

import sn.kubejara.observable.ObservableImpl;
import sn.kubejara.observer.Observer;
import sn.kubejara.observer.ObserverImpl1;
import sn.kubejara.observer.ObserverImpl2;

public class Main {
    public static void main(String[] args) {
        ObservableImpl observable = new ObservableImpl();
        Observer observer1 = new ObserverImpl1();
        Observer observer2 = new ObserverImpl1();
        Observer observer3 = new ObserverImpl2();

        observable.addObserver(observer1);
        observable.addObserver(observer2);
        observable.addObserver(observer3);

        observable.setState(10);
    }
}