package sn.kubejara.observer;

import sn.kubejara.observable.Observable;
import sn.kubejara.observable.ObservableImpl;

public class ObserverImpl1 implements  Observer{
    @Override
    public void update(Observable observable) {

        int state = ((ObservableImpl)observable).getState();
        // On fait le traitement avec l'information reçue de l'observable

        String best_regards = """
                %d
                Oups!
                Ca a encore changé.
                
                Cordialement,
                %s
                """;

        System.out.printf((best_regards) + "%n",state,this.getClass().getName());
    }
}
