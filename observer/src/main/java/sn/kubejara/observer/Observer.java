package sn.kubejara.observer;

import sn.kubejara.observable.Observable;

public interface Observer {
    void update(Observable observable); // On prend en entrée l'Observable qui a fait l'update
}
